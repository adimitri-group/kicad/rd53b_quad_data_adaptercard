EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Data Adapter Card with 2 lines per FE and CMD from DAQ"
Date "2020-12-22"
Rev "Rev 1.0"
Comp "LBNL"
Comment1 "Aleksandra Dimitrievska"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L conn_5025983993:ConnSingle J_conn1
U 1 1 5F90C7EB
P 6100 4950
F 0 "J_conn1" H 6200 7437 60  0000 C CNN
F 1 "ConnSingle" H 6200 7331 60  0000 C CNN
F 2 "Library:Molex-502598-3993" H 6100 4450 60  0001 C CNN
F 3 "" H 6100 4450 60  0001 C CNN
	1    6100 4950
	0    -1   -1   0   
$EndComp
$Comp
L display_port:DISPLAY_PORT DP1
U 1 1 5F90F159
P 1700 1050
F 0 "DP1" V 2137 892 60  0000 C CNN
F 1 "DISPLAY_PORT" V 2031 892 60  0000 C CNN
F 2 "Library:DP1RD20JQ1R400" H 1650 1050 60  0001 C CNN
F 3 "" H 1650 1050 60  0000 C CNN
	1    1700 1050
	0    -1   -1   0   
$EndComp
$Comp
L display_port:DISPLAY_PORT DP2
U 1 1 5F9116DA
P 4350 1050
F 0 "DP2" V 4787 892 60  0000 C CNN
F 1 "DISPLAY_PORT" V 4681 892 60  0000 C CNN
F 2 "Library:DP1RD20JQ1R400" H 4300 1050 60  0001 C CNN
F 3 "" H 4300 1050 60  0000 C CNN
	1    4350 1050
	0    -1   -1   0   
$EndComp
$Comp
L display_port:DISPLAY_PORT DP3
U 1 1 5F912924
P 7000 1050
F 0 "DP3" V 7437 892 60  0000 C CNN
F 1 "DISPLAY_PORT" V 7331 892 60  0000 C CNN
F 2 "Library:DP1RD20JQ1R400" H 6950 1050 60  0001 C CNN
F 3 "" H 6950 1050 60  0000 C CNN
	1    7000 1050
	0    -1   -1   0   
$EndComp
$Comp
L display_port:DISPLAY_PORT DP4
U 1 1 5F9130D4
P 9600 1050
F 0 "DP4" V 10037 892 60  0000 C CNN
F 1 "DISPLAY_PORT" V 9931 892 60  0000 C CNN
F 2 "Library:DP1RD20JQ1R400" H 9550 1050 60  0001 C CNN
F 3 "" H 9550 1050 60  0000 C CNN
	1    9600 1050
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J4
U 1 1 5F91BDA3
P 1850 6250
F 0 "J4" H 1900 6567 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 1900 6476 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04_Pitch2.54mm" H 1850 6250 50  0001 C CNN
F 3 "~" H 1850 6250 50  0001 C CNN
	1    1850 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 5550 4100 6350
Wire Wire Line
	4300 5550 4300 6350
Wire Wire Line
	4500 5550 4500 6350
Wire Wire Line
	4700 5550 4700 6350
Wire Wire Line
	4900 5550 4900 6350
Wire Wire Line
	5100 5550 5100 6350
Wire Wire Line
	5300 5550 5300 6350
Wire Wire Line
	5500 5550 5500 6350
Wire Wire Line
	5700 5550 5700 6350
Wire Wire Line
	5900 5550 5900 6350
Wire Wire Line
	6100 5550 6100 6350
Wire Wire Line
	6300 5550 6300 6350
Wire Wire Line
	6500 5550 6500 6350
Wire Wire Line
	6700 5550 6700 6350
Wire Wire Line
	6900 5550 6900 6350
Wire Wire Line
	7100 5550 7100 6350
Wire Wire Line
	7300 5550 7300 6350
Wire Wire Line
	7500 5550 7500 6350
Wire Wire Line
	7700 5550 7700 6350
Wire Wire Line
	4000 3350 4000 4150
Wire Wire Line
	4200 3350 4200 4150
Wire Wire Line
	4600 3350 4600 4150
Wire Wire Line
	4800 3350 4800 4150
Wire Wire Line
	5000 3350 5000 4150
Wire Wire Line
	5200 3350 5200 4150
Wire Wire Line
	5400 3350 5400 4150
Wire Wire Line
	5600 3350 5600 4150
Wire Wire Line
	5800 3350 5800 4150
Wire Wire Line
	6000 3350 6000 4150
Wire Wire Line
	6200 3350 6200 4150
Wire Wire Line
	6400 3350 6400 4150
Wire Wire Line
	6600 3350 6600 4150
Wire Wire Line
	6800 3350 6800 4150
Wire Wire Line
	7000 3350 7000 4150
Wire Wire Line
	7200 3350 7200 4150
Wire Wire Line
	7400 3350 7400 4150
Wire Wire Line
	7600 3350 7600 4150
Text Label 4100 6350 1    50   ~ 0
LVDS0_Chip1_P
Text Label 4300 6350 1    50   ~ 0
GTX0_Chip2_N
Text Label 4500 6350 1    50   ~ 0
GND
Text Label 4700 6350 1    50   ~ 0
GTX3_Chip1_N
Text Label 4900 6350 1    50   ~ 0
GTX1_Chip2_N
Text Label 5100 6350 1    50   ~ 0
GND
Text Label 5300 6350 1    50   ~ 0
GTX2_Chip1_N
Text Notes 650  7000 0    50   ~ 0
3 types of adapter card:\n- 4 DisplayPort with 2 lines per FE + CMD from DAQ\n- 1 DisplayPort with 1 lines per FE + CMD from DAQ\n- 1 DisplayPort with 1 lines per FE + fwd CMD\n
$Comp
L display_port:DISPLAY_PORT DP5
U 1 1 5F92A373
P 10650 3500
F 0 "DP5" H 10928 3395 60  0000 L CNN
F 1 "DISPLAY_PORT" H 10928 3289 60  0000 L CNN
F 2 "Library:DP1RD20JQ1R400" H 10600 3500 60  0001 C CNN
F 3 "" H 10600 3500 60  0000 C CNN
	1    10650 3500
	1    0    0    -1  
$EndComp
$Comp
L display_port:DISPLAY_PORT DP6
U 1 1 5F92E8D8
P 10700 6200
F 0 "DP6" H 10978 6095 60  0000 L CNN
F 1 "DISPLAY_PORT" H 10978 5989 60  0000 L CNN
F 2 "Library:DP1RD20JQ1R400" H 10650 6200 60  0001 C CNN
F 3 "" H 10650 6200 60  0000 C CNN
	1    10700 6200
	1    0    0    -1  
$EndComp
Text Notes 650  7150 0    50   ~ 0
ChipID jumbers to module GND\n
Text Notes 650  7450 0    50   ~ 0
Check if DP inverts the polarity of the pins\nPin 12 CMD_P (not inverted)\nPin 10 CMD_N (not inverted)
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J3
U 1 1 5F939AF8
P 1850 5700
F 0 "J3" H 1900 5917 50  0000 C CNN
F 1 "Conn_02x02_Odd_Even" H 1900 5826 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x02_Pitch2.54mm" H 1850 5700 50  0001 C CNN
F 3 "~" H 1850 5700 50  0001 C CNN
	1    1850 5700
	1    0    0    -1  
$EndComp
NoConn ~ 9800 2600
NoConn ~ 9800 2800
NoConn ~ 9800 2900
NoConn ~ 9800 3100
NoConn ~ 9800 3200
NoConn ~ 9800 3400
NoConn ~ 9800 3800
NoConn ~ 9800 3900
NoConn ~ 9800 4000
NoConn ~ 9800 4200
NoConn ~ 9800 4300
NoConn ~ 9800 4400
NoConn ~ 9800 4500
NoConn ~ 9850 5300
NoConn ~ 9850 5500
NoConn ~ 9850 5600
NoConn ~ 9850 5800
NoConn ~ 9850 5900
NoConn ~ 9850 6100
NoConn ~ 9850 6500
NoConn ~ 9850 6600
NoConn ~ 9850 6700
NoConn ~ 9850 6900
NoConn ~ 9850 7000
NoConn ~ 9850 7100
NoConn ~ 9850 7200
Text Label 5500 6350 1    50   ~ 0
CHIP_ID3
Text Label 5700 6350 1    50   ~ 0
LP_Enable_Module
Text Label 6000 3350 3    50   ~ 0
GTX2_Chip3_N
Text Label 6500 6350 1    50   ~ 0
GND
Text Label 6400 3350 3    50   ~ 0
GTX1_Chip4_N
Text Label 6600 3350 3    50   ~ 0
GTX3_Chip3_N
Text Label 6800 3350 3    50   ~ 0
GND
Text Label 7000 3350 3    50   ~ 0
GTX0_Chip4_N
Text Label 7100 6350 1    50   ~ 0
GND
Text Label 7300 6350 1    50   ~ 0
Module_CMD_P
Text Label 7500 6350 1    50   ~ 0
LVDS0_Chip3_P
Text Label 7700 6350 1    50   ~ 0
MUX_Chip4
Text Label 4000 3350 3    50   ~ 0
LVDS0_Chip1_N
Text Label 4200 3350 3    50   ~ 0
GND
Text Label 4400 3350 3    50   ~ 0
GTX0_Chip2_P
Text Label 4600 3350 3    50   ~ 0
GTX3_Chip1_P
Text Label 4800 3350 3    50   ~ 0
GND
Text Label 5000 3350 3    50   ~ 0
GTX1_Chip2_P
Text Label 5200 3350 3    50   ~ 0
GTX2_Chip1_P
Text Label 5400 3350 3    50   ~ 0
GND_Module
Text Label 5600 3350 3    50   ~ 0
CHIP_ID2
Text Label 5800 3350 3    50   ~ 0
MUX_Chip1
Text Label 6100 6350 1    50   ~ 0
GTX2_Chip3_P
Text Label 6300 6350 1    50   ~ 0
GTX1_Chip4_P
Text Label 6200 3350 3    50   ~ 0
GND
Text Label 5900 6350 1    50   ~ 0
MUX_Chip3
Text Label 6700 6350 1    50   ~ 0
GTX3_Chip3_P
Text Label 6900 6350 1    50   ~ 0
GTX0_Chip4_P
Text Label 7200 3350 3    50   ~ 0
Module_CMD_N
Text Label 7400 3350 3    50   ~ 0
GND
Text Label 7600 3350 3    50   ~ 0
LVDS0_Chip3_N
Wire Wire Line
	2500 1900 2500 2400
Text Label 2500 2400 1    50   ~ 0
LP_Enable
Wire Wire Line
	10100 1900 10100 2400
Text Label 10100 2400 1    50   ~ 0
Module_CMD_N
Wire Wire Line
	10300 1900 10300 2400
Text Label 10300 2400 1    50   ~ 0
Module_CMD_P
Text Label 9200 3500 0    50   ~ 0
LVDS0_Chip3_N
Text Label 9200 3700 0    50   ~ 0
LVDS0_Chip3_P
Wire Wire Line
	9200 3700 9800 3700
Wire Wire Line
	9200 3500 9800 3500
Text Label 9250 6200 0    50   ~ 0
LVDS0_Chip1_N
Text Label 9250 6400 0    50   ~ 0
LVDS0_Chip1_P
Wire Wire Line
	9250 6400 9850 6400
Wire Wire Line
	9250 6200 9850 6200
Wire Wire Line
	9800 3600 9200 3600
Text Label 9200 3600 0    50   ~ 0
GND
Text Notes 650  7750 0    50   ~ 0
0 Ohm resistor for LP enable for the DAQ\ncheck the analog monitor card for the power and data adatper cards so that it is compatible\n\n
Text Label 1300 2400 1    50   ~ 0
GTX2_Chip1_N
Wire Wire Line
	800  1900 800  2400
Text Label 1100 2400 1    50   ~ 0
GTX2_Chip1_P
Wire Wire Line
	1000 1900 1000 2400
Text Label 1000 2400 1    50   ~ 0
GTX3_Chip1_N
Wire Wire Line
	1100 1900 1100 2400
Text Label 800  2400 1    50   ~ 0
GTX3_Chip1_P
Wire Wire Line
	1300 1900 1300 2400
NoConn ~ 1400 1900
NoConn ~ 1600 1900
NoConn ~ 1700 1900
NoConn ~ 1900 1900
NoConn ~ 2000 1900
NoConn ~ 2100 1900
NoConn ~ 2600 1900
NoConn ~ 2700 1900
NoConn ~ 5150 1900
NoConn ~ 5250 1900
NoConn ~ 5350 1900
NoConn ~ 5050 1900
NoConn ~ 4850 1900
NoConn ~ 4750 1900
NoConn ~ 4650 1900
NoConn ~ 6700 1900
NoConn ~ 6900 1900
NoConn ~ 7000 1900
NoConn ~ 7200 1900
NoConn ~ 7300 1900
NoConn ~ 7400 1900
NoConn ~ 7500 1900
NoConn ~ 7700 1900
NoConn ~ 7800 1900
NoConn ~ 7900 1900
NoConn ~ 8000 1900
NoConn ~ 9900 1900
NoConn ~ 10000 1900
NoConn ~ 10400 1900
NoConn ~ 10500 1900
NoConn ~ 10600 1900
Text Label 4550 2400 1    50   ~ 0
GTX0_Chip2_N
Wire Wire Line
	4550 1900 4550 2400
Text Label 4350 2400 1    50   ~ 0
GTX0_Chip2_P
Wire Wire Line
	4350 1900 4350 2400
Text Label 4250 2400 1    50   ~ 0
GTX1_Chip2_N
Wire Wire Line
	4250 1900 4250 2400
Text Label 4050 2400 1    50   ~ 0
GTX1_Chip2_P
Wire Wire Line
	4050 1900 4050 2400
Text Label 6600 2400 1    50   ~ 0
GTX2_Chip3_N
Wire Wire Line
	6100 1900 6100 2400
Text Label 6400 2400 1    50   ~ 0
GTX2_Chip3_P
Wire Wire Line
	6300 1900 6300 2400
Text Label 6300 2400 1    50   ~ 0
GTX3_Chip3_N
Wire Wire Line
	6400 1900 6400 2400
Text Label 6100 2400 1    50   ~ 0
GTX3_Chip3_P
Wire Wire Line
	6600 1900 6600 2400
Text Label 9800 2400 1    50   ~ 0
GTX0_Chip4_N
Wire Wire Line
	9800 1900 9800 2400
Text Label 9600 2400 1    50   ~ 0
GTX0_Chip4_P
Wire Wire Line
	9600 1900 9600 2400
Text Label 9500 2400 1    50   ~ 0
GTX1_Chip4_N
Wire Wire Line
	9500 1900 9500 2400
Text Label 9300 2400 1    50   ~ 0
GTX1_Chip4_P
Wire Wire Line
	9300 1900 9300 2400
Wire Wire Line
	9850 6300 9250 6300
Text Label 9250 6300 0    50   ~ 0
GND
Wire Wire Line
	1650 5700 850  5700
Text Label 850  5700 0    50   ~ 0
CHIP_ID2
Wire Wire Line
	1650 5800 850  5800
Text Label 850  5800 0    50   ~ 0
CHIP_ID3
Wire Wire Line
	2950 5700 2150 5700
Text Label 2950 5700 2    50   ~ 0
GND_Module
Wire Wire Line
	2950 5800 2150 5800
Text Label 2950 5800 2    50   ~ 0
GND_Module
Text Label 3900 6350 1    50   ~ 0
MUX_Chip2
Wire Wire Line
	3900 5550 3900 6350
Text Label 850  6250 0    50   ~ 0
MUX_Chip2
Wire Wire Line
	1650 6250 850  6250
Text Label 850  6150 0    50   ~ 0
MUX_Chip1
Wire Wire Line
	1650 6150 850  6150
Text Label 850  6450 0    50   ~ 0
MUX_Chip4
Wire Wire Line
	1650 6450 850  6450
Text Label 850  6350 0    50   ~ 0
MUX_Chip3
Wire Wire Line
	1650 6350 850  6350
Wire Wire Line
	2950 6150 2150 6150
Text Label 2950 6150 2    50   ~ 0
GND_Module
Wire Wire Line
	2950 6250 2150 6250
Text Label 2950 6250 2    50   ~ 0
GND_Module
Wire Wire Line
	2950 6350 2150 6350
Text Label 2950 6350 2    50   ~ 0
GND_Module
Wire Wire Line
	2950 6450 2150 6450
Text Label 2950 6450 2    50   ~ 0
GND_Module
$Comp
L Device:R R2
U 1 1 5FCA2E69
P 4400 3200
F 0 "R2" H 4470 3246 50  0000 L CNN
F 1 "10k" H 4470 3155 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 4330 3200 50  0001 C CNN
F 3 "~" H 4400 3200 50  0001 C CNN
	1    4400 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3350 4400 4150
Wire Wire Line
	4400 3050 4400 2850
Text Label 4400 2850 3    50   ~ 0
GND
$Comp
L Device:R R3
U 1 1 5FCA8125
P 4600 3200
F 0 "R3" H 4670 3246 50  0000 L CNN
F 1 "10k" H 4670 3155 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 4530 3200 50  0001 C CNN
F 3 "~" H 4600 3200 50  0001 C CNN
	1    4600 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3050 4600 2850
Text Label 4600 2850 3    50   ~ 0
GND
$Comp
L Device:R R6
U 1 1 5FCAAC0D
P 5000 3200
F 0 "R6" H 5070 3246 50  0000 L CNN
F 1 "10k" H 5070 3155 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 4930 3200 50  0001 C CNN
F 3 "~" H 5000 3200 50  0001 C CNN
	1    5000 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3050 5000 2850
Text Label 5000 2850 3    50   ~ 0
GND
$Comp
L Device:R R7
U 1 1 5FCAAC19
P 5200 3200
F 0 "R7" H 5270 3246 50  0000 L CNN
F 1 "10k" H 5270 3155 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 5130 3200 50  0001 C CNN
F 3 "~" H 5200 3200 50  0001 C CNN
	1    5200 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3050 5200 2850
Text Label 5200 2850 3    50   ~ 0
GND
$Comp
L Device:R R9
U 1 1 5FCADBF1
P 6000 3200
F 0 "R9" H 6070 3246 50  0000 L CNN
F 1 "10k" H 6070 3155 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 5930 3200 50  0001 C CNN
F 3 "~" H 6000 3200 50  0001 C CNN
	1    6000 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3050 6000 2850
Text Label 6000 2850 3    50   ~ 0
GND
$Comp
L Device:R R12
U 1 1 5FCB4278
P 6400 3200
F 0 "R12" H 6470 3246 50  0000 L CNN
F 1 "10k" H 6470 3155 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 6330 3200 50  0001 C CNN
F 3 "~" H 6400 3200 50  0001 C CNN
	1    6400 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3050 6400 2850
Text Label 6400 2850 3    50   ~ 0
GND
$Comp
L Device:R R13
U 1 1 5FCB4284
P 6600 3200
F 0 "R13" H 6670 3246 50  0000 L CNN
F 1 "10k" H 6670 3155 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 6530 3200 50  0001 C CNN
F 3 "~" H 6600 3200 50  0001 C CNN
	1    6600 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 3050 6600 2850
Text Label 6600 2850 3    50   ~ 0
GND
$Comp
L Device:R R16
U 1 1 5FCB7F61
P 7000 3200
F 0 "R16" H 7070 3246 50  0000 L CNN
F 1 "10k" H 7070 3155 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 6930 3200 50  0001 C CNN
F 3 "~" H 7000 3200 50  0001 C CNN
	1    7000 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3050 7000 2850
Text Label 7000 2850 3    50   ~ 0
GND
$Comp
L Device:R R5
U 1 1 5FCBE2E9
P 4900 6500
F 0 "R5" H 4970 6546 50  0000 L CNN
F 1 "10k" H 4970 6455 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 4830 6500 50  0001 C CNN
F 3 "~" H 4900 6500 50  0001 C CNN
	1    4900 6500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4900 6650 4900 6850
Text Label 4900 6850 1    50   ~ 0
GND
$Comp
L Device:R R4
U 1 1 5FCBE2F5
P 4700 6500
F 0 "R4" H 4770 6546 50  0000 L CNN
F 1 "10k" H 4770 6455 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 4630 6500 50  0001 C CNN
F 3 "~" H 4700 6500 50  0001 C CNN
	1    4700 6500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4700 6650 4700 6850
Text Label 4700 6850 1    50   ~ 0
GND
$Comp
L Device:R R11
U 1 1 5FCC289E
P 6300 6500
F 0 "R11" H 6370 6546 50  0000 L CNN
F 1 "10k" H 6370 6455 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 6230 6500 50  0001 C CNN
F 3 "~" H 6300 6500 50  0001 C CNN
	1    6300 6500
	-1   0    0    1   
$EndComp
Wire Wire Line
	6300 6650 6300 6850
Text Label 6300 6850 1    50   ~ 0
GND
$Comp
L Device:R R10
U 1 1 5FCC28AA
P 6100 6500
F 0 "R10" H 6170 6546 50  0000 L CNN
F 1 "10k" H 6170 6455 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 6030 6500 50  0001 C CNN
F 3 "~" H 6100 6500 50  0001 C CNN
	1    6100 6500
	-1   0    0    1   
$EndComp
Wire Wire Line
	6100 6650 6100 6850
Text Label 6100 6850 1    50   ~ 0
GND
$Comp
L Device:R R15
U 1 1 5FCC6900
P 6900 6500
F 0 "R15" H 6970 6546 50  0000 L CNN
F 1 "10k" H 6970 6455 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 6830 6500 50  0001 C CNN
F 3 "~" H 6900 6500 50  0001 C CNN
	1    6900 6500
	-1   0    0    1   
$EndComp
Wire Wire Line
	6900 6650 6900 6850
$Comp
L Device:R R14
U 1 1 5FCC690B
P 6700 6500
F 0 "R14" H 6770 6546 50  0000 L CNN
F 1 "10k" H 6770 6455 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 6630 6500 50  0001 C CNN
F 3 "~" H 6700 6500 50  0001 C CNN
	1    6700 6500
	-1   0    0    1   
$EndComp
Wire Wire Line
	6700 6650 6700 6850
Text Label 6700 6850 1    50   ~ 0
GND
Text Label 6900 6850 1    50   ~ 0
GND
$Comp
L Device:R R8
U 1 1 5FCCAB71
P 5300 6500
F 0 "R8" H 5370 6546 50  0000 L CNN
F 1 "10k" H 5370 6455 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 5230 6500 50  0001 C CNN
F 3 "~" H 5300 6500 50  0001 C CNN
	1    5300 6500
	-1   0    0    1   
$EndComp
Wire Wire Line
	5300 6650 5300 6850
Text Label 5300 6850 1    50   ~ 0
GND
$Comp
L Device:R R1
U 1 1 5FCCE5F9
P 4300 6500
F 0 "R1" H 4370 6546 50  0000 L CNN
F 1 "10k" H 4370 6455 50  0000 L CNN
F 2 "Resistors_SMD:R_0402_NoSilk" V 4230 6500 50  0001 C CNN
F 3 "~" H 4300 6500 50  0001 C CNN
	1    4300 6500
	-1   0    0    1   
$EndComp
Wire Wire Line
	4300 6650 4300 6850
Text Label 4300 6850 1    50   ~ 0
GND
Text Label 10200 2400 1    50   ~ 0
GND
Wire Wire Line
	10200 1900 10200 2400
Text Label 9700 2400 1    50   ~ 0
GND
Wire Wire Line
	9700 1900 9700 2400
Text Label 9400 2400 1    50   ~ 0
GND
Wire Wire Line
	9400 1900 9400 2400
Text Label 9100 2400 1    50   ~ 0
GND
Wire Wire Line
	9100 1900 9100 2400
Text Label 8800 2400 1    50   ~ 0
GND
Wire Wire Line
	8800 1900 8800 2400
Text Label 7600 2400 1    50   ~ 0
GND
Wire Wire Line
	7600 1900 7600 2400
Text Label 7100 2400 1    50   ~ 0
GND
Wire Wire Line
	7100 1900 7100 2400
Text Label 6800 2400 1    50   ~ 0
GND
Wire Wire Line
	6800 1900 6800 2400
Text Label 6500 2400 1    50   ~ 0
GND
Wire Wire Line
	6500 1900 6500 2400
Text Label 6200 2400 1    50   ~ 0
GND
Wire Wire Line
	6200 1900 6200 2400
Text Label 4950 2400 1    50   ~ 0
GND
Wire Wire Line
	4950 1900 4950 2400
Text Label 4450 2400 1    50   ~ 0
GND
Wire Wire Line
	4450 1900 4450 2400
Text Label 4150 2400 1    50   ~ 0
GND
Wire Wire Line
	4150 1900 4150 2400
Text Label 3850 2400 1    50   ~ 0
GND
Wire Wire Line
	3850 1900 3850 2400
Text Label 3550 2400 1    50   ~ 0
GND
Wire Wire Line
	3550 1900 3550 2400
Text Label 2300 2400 1    50   ~ 0
GND
Wire Wire Line
	2300 1900 2300 2400
Text Label 1800 2400 1    50   ~ 0
GND
Wire Wire Line
	1800 1900 1800 2400
Text Label 1500 2400 1    50   ~ 0
GND
Wire Wire Line
	1500 1900 1500 2400
Text Label 1200 2400 1    50   ~ 0
GND
Wire Wire Line
	1200 1900 1200 2400
Text Label 900  2400 1    50   ~ 0
GND
Wire Wire Line
	900  1900 900  2400
$Comp
L Connector_Generic:Conn_02x01 J2
U 1 1 5FD3F733
P 1850 5350
F 0 "J2" H 1900 5567 50  0000 C CNN
F 1 "Conn_02x01" H 1900 5476 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 1850 5350 50  0001 C CNN
F 3 "~" H 1850 5350 50  0001 C CNN
	1    1850 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 5350 1150 5350
Text Label 1150 5350 0    50   ~ 0
LP_Enable
Wire Wire Line
	2150 5350 2950 5350
Text Label 2950 5350 2    50   ~ 0
LP_Enable_Module
Wire Wire Line
	3050 1250 3050 1700
Wire Wire Line
	5700 1250 5700 1700
Wire Wire Line
	8350 1250 8350 1700
Wire Wire Line
	10950 1250 10950 1700
Wire Wire Line
	10450 4850 10000 4850
Wire Wire Line
	10500 7550 10050 7550
Text Label 10050 7550 0    50   ~ 0
GND_D6
Text Label 10000 4850 0    50   ~ 0
GND_D5
Text Label 10950 1700 1    50   ~ 0
GND_D4
Text Label 8350 1700 1    50   ~ 0
GND_D3
Text Label 5700 1700 1    50   ~ 0
GND_D2
Text Label 3050 1700 1    50   ~ 0
GND_D1
$Comp
L Device:C C1
U 1 1 5FDA3296
P 3050 1850
F 0 "C1" V 2798 1850 50  0000 C CNN
F 1 "C" V 2889 1850 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" H 3088 1700 50  0001 C CNN
F 3 "~" H 3050 1850 50  0001 C CNN
	1    3050 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	3050 2000 3050 2200
Text Label 3050 2200 1    50   ~ 0
GND
$Comp
L Device:C C2
U 1 1 5FDA984D
P 5700 1850
F 0 "C2" V 5448 1850 50  0000 C CNN
F 1 "C" V 5539 1850 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" H 5738 1700 50  0001 C CNN
F 3 "~" H 5700 1850 50  0001 C CNN
	1    5700 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	5700 2000 5700 2200
Text Label 5700 2200 1    50   ~ 0
GND
$Comp
L Device:C C3
U 1 1 5FDAFD04
P 8350 1850
F 0 "C3" V 8098 1850 50  0000 C CNN
F 1 "C" V 8189 1850 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" H 8388 1700 50  0001 C CNN
F 3 "~" H 8350 1850 50  0001 C CNN
	1    8350 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	8350 2000 8350 2200
Text Label 8350 2200 1    50   ~ 0
GND
$Comp
L Device:C C6
U 1 1 5FDB61EC
P 10950 1850
F 0 "C6" V 10698 1850 50  0000 C CNN
F 1 "C" V 10789 1850 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" H 10988 1700 50  0001 C CNN
F 3 "~" H 10950 1850 50  0001 C CNN
	1    10950 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	10950 2000 10950 2200
Text Label 10950 2200 1    50   ~ 0
GND
$Comp
L Device:C C4
U 1 1 5FDBCFCF
P 9850 4850
F 0 "C4" V 9598 4850 50  0000 C CNN
F 1 "C" V 9689 4850 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" H 9888 4700 50  0001 C CNN
F 3 "~" H 9850 4850 50  0001 C CNN
	1    9850 4850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9700 4850 9500 4850
Text Label 9500 4850 0    50   ~ 0
GND
$Comp
L Device:C C5
U 1 1 5FDC3931
P 9900 7550
F 0 "C5" V 9648 7550 50  0000 C CNN
F 1 "C" V 9739 7550 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" H 9938 7400 50  0001 C CNN
F 3 "~" H 9900 7550 50  0001 C CNN
	1    9900 7550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9750 7550 9550 7550
Text Label 9550 7550 0    50   ~ 0
GND
Wire Notes Line
	500  6600 500  7750
Wire Notes Line
	4050 6600 4050 7750
Wire Notes Line
	500  7750 4050 7750
Wire Notes Line
	500  6600 4050 6600
NoConn ~ 3950 1900
NoConn ~ 3750 1900
NoConn ~ 3650 1900
NoConn ~ 3450 1900
Wire Wire Line
	9800 2700 9200 2700
Text Label 9200 2700 0    50   ~ 0
GND
Wire Wire Line
	9800 3000 9200 3000
Text Label 9200 3000 0    50   ~ 0
GND
Wire Wire Line
	9800 3300 9200 3300
Text Label 9200 3300 0    50   ~ 0
GND
Wire Wire Line
	9800 4100 9200 4100
Text Label 9200 4100 0    50   ~ 0
GND
NoConn ~ 9200 1900
NoConn ~ 9000 1900
NoConn ~ 8900 1900
NoConn ~ 8700 1900
Wire Wire Line
	9850 5400 9250 5400
Text Label 9250 5400 0    50   ~ 0
GND
Wire Wire Line
	9850 5700 9250 5700
Text Label 9250 5700 0    50   ~ 0
GND
Wire Wire Line
	9850 6000 9250 6000
Text Label 9250 6000 0    50   ~ 0
GND
Wire Wire Line
	9850 6800 9250 6800
Text Label 9250 6800 0    50   ~ 0
GND
NoConn ~ 2200 1900
NoConn ~ 2400 1900
$Comp
L Connector_Generic:Conn_02x01 J1
U 1 1 600102F3
P 1850 5000
F 0 "J1" H 1900 5217 50  0000 C CNN
F 1 "Conn_02x01" H 1900 5126 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 1850 5000 50  0001 C CNN
F 3 "~" H 1850 5000 50  0001 C CNN
	1    1850 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 5000 1150 5000
Text Label 1150 5000 0    50   ~ 0
GND
Wire Wire Line
	2150 5000 2950 5000
Text Label 2950 5000 2    50   ~ 0
GND
$EndSCHEMATC
